A fim de documentar nossa avaliação final da disciplina de Programação para Internet I, atividade
esta que representa 60% do total de pontos a serem distribuídos, registram-se as devidas
orientações.
O trabalho consiste em desenvolver e apresentar um site web que deve:
 respeitar o tema e funcionalidades definidas: E-comerce / loja virtual na qual os produtos são
exibidos, e o administrador do site poder cadastrar, editar e excluir produtos;
 cada produto deve pertencer a uma categoria, e os produtos devem poder ser listados por
categoria, assim como individualmente;
 Não há necessidade de restringir o acesso ao administrador, porém, o mesmo deve possuir
uma área própria para gerenciamento, assim como não há necessidade de implementar o
carrinho de compras;
 ser desenvolvido utilizando, obrigatoriamente, todas as seguintes linguagens / tecnologias:
HTML, Javascript, CSS, PHP, MySQL e Bootstrap 4. Outras linguagens, tecnologias e
frameworks adicionais podem ser utilizados, desde que com prévia anuência do professor;
 realizar a conexão no banco de dados através da biblioteca PDO; e
 apresentar boa usabilidade, ser totalmente responsivo, com layout limpo e eficiente, a fim de
facilitar a navegação do usuário;
Atenção: o não atendimento a qualquer uma das orientações acima invalida o trabalho, que terá nota
zero atribuída. Em caso de qualquer dúvida, esclareça-as com o professor.
Quanto à entrega, apresentação e demais detalhes.
 A atividade é individual;
 O software deverá ser disponibilizado no gitlab, e o link do projeto (público) enviado para
chrismanuel@iftm.edu.br até às 18h59min do dia 02/07/2019. Após este prazo, eventual
envio será desconsiderado;
 Após o envio, o projeto não deverá sofrer qualquer tipo de alteração;
 O assunto do e-mail de entrega deverá conter: “PPI I 2019-01 Trabalho final”. Revise bem o
seu projeto ANTES de enviar, pois será considerado apenas 01 e-mail por aluno;
 O projeto, no gitlab, deverá apresentar na raiz o script SQL do banco de dados. >> Atenção >>
Teste o seu script SQL (importando-o) em outras máquinas, a fim de garantir que este não irá
apresentar problemas. Caso o mesmo apresente problemas de importação, o trabalho terá
nota zero atribuída;
 O código-fonte não deverá conter nenhum tipo de comentário;
 Nos dias 02 e 03/07/19 os trabalhos deverão ser apresentados em sala de aula, em ordem a
ser definida nas respectivas datas;
 Cada trabalho deverá ser apresentando em no máximo 10min;
 Após a apresentação ocorrerá arguição oral; e
 Além das orientações já mencionadas, será critério de avaliação a legibilidade do código, o
atendimento aos requisitos propostos, usabilidade, entendimento pleno sobre o código
fonte.
Não deixe eventual dúvida sem resposta. Esclareça-as em tempo hábil com o professor.
